module InContactClient
  module Resources
    AGENTS         = "Agents"
    AGENT_SESSIONS = "AgentSessions"
    CONTACTS       = "Contacts"
  end
end
